package com.mitocode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MockTransferenciasEntreCuentasTest {

	@Mock
	private Cuenta cuentaOrigen;
	
	@Mock
	private Cuenta cuentaDestino;
	
	
	@Test
	public void transferenciaEntreCuentas() throws CuentaException {

		doReturn(5000.00).when(cuentaOrigen).getMonto();
		doReturn(1000.00).when(cuentaDestino).getMonto();
		
		assertEquals(cuentaOrigen.getMonto(), 5000.00, 0);
		assertEquals(cuentaDestino.getMonto(), 1000.00, 0);
		
		when(cuentaOrigen.tranferencia(cuentaDestino, 2000.00)).thenReturn(true);
		
		assertEquals(cuentaOrigen.tranferencia(cuentaDestino, 2000.00), true);


//		assertTrue(cuentaOrigen.tranferencia(cuentaDestino, 2000.00));

	}
}
